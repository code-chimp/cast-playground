module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 8,
    sourceType: 'module',
    ecmaFeatures: {
      impliedStrict: true
    }
  },
  env: {
    browser: true,
    node: true,
    es6: true,
    jasmine: true
  },
  plugins: [
    'import',
    'node',
    'promise',
    'jasmine'
  ],
  extends: [
    'standard',
    'semistandard',
    'plugin:jasmine/recommended'
  ]
};
