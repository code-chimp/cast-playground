﻿module.exports = ctx => ({
  plugins: [
    require('autoprefixer')({
      browsers: ['> 0.0001%'],
      cascade: true,
      remove: true
    }),
    require('css-mqpacker')(),
    require('cssnano')
  ].filter(e => e !== null)
});
