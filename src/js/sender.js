/* global chrome */
import { APP_ID, NAMESPACE } from './constants';

let session = null;

if (!chrome.cast || !chrome.cast.isAvailable) {
  setTimeout(initializeCastApi, 1000);
}

document.getElementById('ccForm').addEventListener('submit', formSubmit);

function initializeCastApi () {
  const sessionRequest = new chrome.cast.SessionRequest(APP_ID);
  const apiConfig = new chrome.cast.ApiConfig(
    sessionRequest,
    sessionListener,
    receiverListener);
  chrome.cast.initialize(apiConfig, onInitSuccess, onError);
}

function onInitSuccess () {
  appendMessage('onInitSuccess');
}

function onError (message) {
  appendMessage(`onError: ${JSON.stringify(message)}`);
}

function onSuccess (message) {
  appendMessage(`onSuccess: ${message}`);
}

function onStopAppSuccess () {
  appendMessage('onStopAppSuccess');
}

function sessionListener (e) {
  appendMessage(`New session ID: ${e.sessionId}`);
  session = e;
  session.addUpdateListener(sessionUpdateListener);
  session.addMessageListener(NAMESPACE, receiverMessage);
}

function sessionUpdateListener (isAlive) {
  let message = isAlive
    ? 'Session Updated'
    : 'Session Removed';
  message += `: ${session.sessionId}`;
  appendMessage(message);

  if (!isAlive) {
    session = null;
  }
}

function receiverMessage (namespace, message) {
  appendMessage(`receiverMessage: ${namespace}, ${message}`);
}

function receiverListener (e) {
  if (e === 'available') {
    appendMessage('receiver found');
  } else {
    appendMessage('receiver list empty');
  }
}

/* eslint-disable */
function stopApp () {
  session.stop(onStopAppSuccess, onError);
}
/* eslint-enable */

function sendMessage (message) {
  if (session != null) {
    session.sendMessage(
      NAMESPACE,
      message,
      onSuccess.bind(this, `Message sent: ${message}`),
      onError);
  } else {
    chrome.cast.requestSession(function (e) {
      session = e;
      session.sendMessage(
        NAMESPACE,
        message,
        onSuccess.bind(this, `Message sent: ${message}`),
        onError);
    }, onError);
  }
}

function appendMessage (message) {
  console.log(message);
  document.getElementById('debugmessage')
    .innerHTML += '\n' + JSON.stringify(message);
}

function formSubmit (e) {
  e.preventDefault();
  sendMessage(document.getElementById('ccInput').value);
}

/* eslint-disable */
function transcribe (words) {
  sendMessage(words);
}
/* eslint-enable */
