const webpack = require('webpack');
// const AssetsPlugin = require('assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const { resolve } = require('path');

// useful in dev, bloaty in prod
const DEV_TOOL = process.env.NODE_ENV !== 'production'
  ? 'source-map'
  : false;

const BAIL = process.env.NODE_ENV === 'production';

// mostly useless in dev, especially when doing HMR
const PERFORMANCE_HINTS = process.env.NODE_ENV !== 'production'
  ? false
  : 'warning';

module.exports = {
  target: 'web',

  devtool: DEV_TOOL,
  cache: true,
  bail: BAIL,

  performance: {
    hints: PERFORMANCE_HINTS
  },

  entry: {
    sender: resolve(__dirname, 'src', 'js', 'sender.js'),
    receiver: resolve(__dirname, 'src', 'js', 'receiver.js'),
    sender_style: resolve(__dirname, 'src', 'scss', 'sender.scss'),
    receiver_style: resolve(__dirname, 'src', 'scss', 'receiver.scss')
  },

  output: {
    path: resolve(__dirname, 'dist'),
    filename: '[name].js',
    chunkFilename: '[chunkhash:8].js',
    pathinfo: false
  },

  resolve: {
    extensions: ['.js', '.json']
  },

  stats: {
    colors: true,
    reasons: false,
    chunks: false
  },

  module: {
    noParse: [
      /\.min\.js/
    ],
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        use: [{
          loader: 'eslint-loader',
          options: {}
        }],
        exclude: [resolve(__dirname, 'node_modules')]
      }, {
        test: /\.html$/,
        use: [{ loader: 'raw-loader' }]
      }, {
        test: /\.js$/,
        use: [{
          loader: 'babel-loader',
          options: { cacheDirectory: true }
        }],
        exclude: [resolve(__dirname, 'node_modules')]
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 50000,
            mimetype: 'application/font-woff'
          }
        }],
        include: [
          resolve(__dirname, './node_modules/font-awesome/fonts')
        ]
      }, {
        test: /\.(otf|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [{ loader: 'file-loader' }]
      }, {
        test: /\.(gif|jpg|png)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 25000,
            name: '[name]-[hash:8].[ext]'
          }
        }, {
          loader: 'image-webpack-loader',
          options: {
            progressive: true,
            gifsicle: {
              interlaced: false
            },
            optipng: {
              optimizationLevel: 7
            },
            mozjpeg: {
              quality: 65
            },
            pngquant: {
              quality: '65-90',
              speed: 4
            }
          }
        }]
      }, {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [{ loader: 'css-loader' }],
          publicPath: '/'
        })
      }, {
        test: /\.(s+(a|c)ss)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader' },
            { loader: 'postcss-loader' },
            { loader: 'sass-loader' }
          ],
          publicPath: '/'
        })
      }
    ]
  },

  plugins: [
    // set up globals expected by some packages
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default']
    }),

    // packages like React build smaller if in 'production'
    //  supplying 'build' to allow modules to skip importing packages that
    //    may contain globals like 'window' that cause the server bundle to
    //      vomit
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    }),

    // wipe our build directory
    new CleanWebpackPlugin([
      resolve(__dirname, 'dist')
    ], {
      root: process.cwd(),
      dry: false,
      verbose: true,
      exclude: ['.gitkeep']
    }),

    // statics do not require any pre-processing
    new CopyWebpackPlugin([{
      from: resolve(__dirname, 'src', 'statics'),
      to: resolve(__dirname, 'dist')
    }], { copyUnmodified: true }),

    // prevents some hashing issues with the bundle cache-busting
    // new AssetsPlugin({
    //   includeManifest: 'manifest',
    //   path: resolve(__dirname, 'src', 'server', 'config'),
    //   prettyPrint: true
    // }),

    // css goes in its own file to avoid FOUC
    new ExtractTextPlugin({
      filename: getPath => {
        return getPath('[name].css').replace('_style', '');
      },
      disable: false,
      allChunks: true
    })
  ]
};
